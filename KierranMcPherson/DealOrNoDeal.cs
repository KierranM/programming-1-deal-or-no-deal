﻿/******************************************************************************************
 * Title: Deal Or No Deal                                                                 *
 * Purpose: To play a game of Deal or No Deal, and offer various menu functionality       *
 * Author: Kierran McPherson                                                              *
 * Date: 29/05/2012                                                                       *
 ******************************************************************************************/
using System;
using System.Linq;
using System.IO;
using System.Threading;

namespace KierranMcPherson
{
    public struct Contestant
    {
        public string lastName;
        public string firstName;
        public string interests;
        public double highScore;
    }

    class DealOrNoDeal
    {
        public static int arraySize;
        public static Contestant[] contestantList = new Contestant[1];
        public static Contestant[] finalists = new Contestant[10];
        public static double[] caseValues = { 0.01, 1, 5, 10, 25, 50, 75, 100, 200, 300, 400, 500, 750, 1000, 5000, 10000, 25000, 50000, 75000, 100000, 200000, 300000, 400000, 500000, 750000, 1000000 };
        public static double[] cases = new double[26];
        public static bool[] casesOpened = new bool[26];
        public static bool[] cashValuesRevealed = new bool[26];
        public static bool finalistsChosen = false;
        public static Contestant thePlayer;
        public static ConsoleKeyInfo keyPress;

        #region PreGame Methods

        #region Menu Frames

        public static void FirstScreen()
        {
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"**   ______           _   ______        __  __         ______           _    **");
            Console.WriteLine(@"**   |  _  \         | |  |  _  |       | \ | |        |  _  \         | |   **");
            Console.WriteLine(@"**   | | | |___  __ _| |  | | | |_ __   |  \| | ___    | | | |___  __ _| |   **");
            Console.WriteLine(@"**   | | | / _ \/ _` | |  | | | | '__|  | . ` |/ _ \   | | | / _ \/ _` | |   **");
            Console.WriteLine(@"**   | |/ /  __/ (_| | |  \ \_/ / |     | |\  | (_) |  | |/ /  __/ (_| | |   **");
            Console.WriteLine(@"**   |___/ \___|\__,_|_|   \___/|_|     \_| \_/\___/   |___/ \___|\__,_|_|   **");
            Console.WriteLine(@"**                                                                           **");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"**                                                                           **");
            Console.WriteLine(@"**             I do not own the 'Deal Or No Deal' Format or Concept.         **");
            Console.WriteLine(@"**   All Copyrights and Trademarks are property of their respective owners.  **");
            Console.WriteLine(@"**                                                                           **");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"*******************************************************************************");
        }
        public static void ShortTitle()
        {
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine(@"**   ______           _   _____         _   _          ______           _    **");
            Console.WriteLine(@"**   |  _  \         | |  |  _  |       | \ | |        |  _  \         | |   **");
            Console.WriteLine(@"**   | | | |___  __ _| |  | | | |_ __   |  \| | ___    | | | |___  __ _| |   **");
            Console.WriteLine(@"**   | | | / _ \/ _` | |  | | | | '__|  | . ` |/ _ \   | | | / _ \/ _` | |   **");
            Console.WriteLine(@"**   | |/ /  __/ (_| | |  \ \_/ / |     | |\  | (_) |  | |/ /  __/ (_| | |   **");
            Console.WriteLine(@"**   |___/ \___|\__,_|_|   \___/|_|     \_| \_/\___/   |___/ \___|\__,_|_|   **");
            Console.WriteLine(@"**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
        }
        public static void Title()
        {
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"**   ______           _   ______        __  __         ______           _    **");
            Console.WriteLine(@"**   |  _  \         | |  |  _  |       | \ | |        |  _  \         | |   **");
            Console.WriteLine(@"**   | | | |___  __ _| |  | | | |_ __   |  \| | ___    | | | |___  __ _| |   **");
            Console.WriteLine(@"**   | | | / _ \/ _` | |  | | | | '__|  | . ` |/ _ \   | | | / _ \/ _` | |   **");
            Console.WriteLine(@"**   | |/ /  __/ (_| | |  \ \_/ / |     | |\  | (_) |  | |/ /  __/ (_| | |   **");
            Console.WriteLine(@"**   |___/ \___|\__,_|_|   \___/|_|     \_| \_/\___/   |___/ \___|\__,_|_|   **");
            Console.WriteLine(@"**                                                                           **");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"**                                                                           **");
            Console.WriteLine(@"**                  Use the Up and Down Arrow Keys to Select                 **");
            Console.WriteLine(@"**                           Press Enter to Confirm                          **");
            Console.WriteLine(@"**                                                                           **");
            Console.WriteLine(@"*******************************************************************************");
            Console.WriteLine(@"*******************************************************************************");
        }

        public static void MenuFrameOne()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.Write("**                           "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write(" Show Contestants "); Console.ForegroundColor = ConsoleColor.White; Console.BackgroundColor = ConsoleColor.Black; Console.WriteLine("                              **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Edit Contestants                               **");
            Console.WriteLine("**                                                                           **");
            if (finalistsChosen == false)
            {
                Console.WriteLine("**                            Choose Finalists                               **");
            }
            else
            {
                Console.WriteLine("**                             Show Finalists                                **");
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                           Clear High Scores                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                               Let's Play                                  **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                             Save And Exit                                 **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
        }
        public static void MenuFrameTwo()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Show Contestants                               **");
            Console.WriteLine("**                                                                           **");
            Console.Write("**                           "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write(" Edit Contestants "); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                              **");
            Console.WriteLine("**                                                                           **");
            if (finalistsChosen == false)
            {
                Console.WriteLine("**                            Choose Finalists                               **");
            }
            else
            {
                Console.WriteLine("**                             Show Finalists                                **");
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                           Clear High Scores                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                               Let's Play                                  **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                             Save And Exit                                 **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
        }
        public static void MenuFrameThree()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Show Contestants                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Edit Contestants                               **");
            Console.WriteLine("**                                                                           **");
            if (finalistsChosen == false)
            {
                Console.Write("**                           "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write(" Choose Finalists "); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                              **");
            }
            else
            {
                Console.Write("**                            "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write(" Show Finalists "); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                               **");
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                           Clear High Scores                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                               Let's Play                                  **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                             Save And Exit                                 **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
        }
        public static void MenuFrameFour()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Show Contestants                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Edit Contestants                               **");
            Console.WriteLine("**                                                                           **");
            if (finalistsChosen == false)
            {
                Console.WriteLine("**                            Choose Finalists                               **");
            }
            else
            {
                Console.WriteLine("**                             Show Finalists                                **");
            }
            Console.WriteLine("**                                                                           **");
            Console.Write("**                          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write(" Clear High Scores "); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                              **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                               Let's Play                                  **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                             Save And Exit                                 **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
        }
        public static void MenuFrameFive()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Show Contestants                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Edit Contestants                               **");
            Console.WriteLine("**                                                                           **");
            if (finalistsChosen == false)
            {
                Console.WriteLine("**                            Choose Finalists                               **");
            }
            else
            {
                Console.WriteLine("**                             Show Finalists                                **");
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                           Clear High Scores                               **");
            Console.WriteLine("**                                                                           **");
            Console.Write("**                              "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write(" Let's Play "); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                                 **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                             Save And Exit                                 **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
        }
        public static void MenuFrameSix()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Show Contestants                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            Edit Contestants                               **");
            Console.WriteLine("**                                                                           **");
            if (finalistsChosen == false)
            {
                Console.WriteLine("**                            Choose Finalists                               **");
            }
            else
            {
                Console.WriteLine("**                             Show Finalists                                **");
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                           Clear High Scores                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                               Let's Play                                  **");
            Console.WriteLine("**                                                                           **");
            Console.Write("**                            "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write(" Save And Exit "); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                                **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
        }
        #endregion Menu Frames

        public static void Menu()
        {
            bool quit = false;

            Console.ForegroundColor = ConsoleColor.White;
            FirstScreen();
            ImportPlayers();
            Thread.Sleep(4000);
            Console.Clear();

            Console.WindowHeight = Console.LargestWindowHeight;

            int choice = 0;
            do
            {
                do
                {

                    switch (choice % 6) //shows each fram depending on the value of count
                    {
                        case 0:

                            Title();
                            MenuFrameOne();

                            break;
                        case 1:

                            Title();
                            MenuFrameTwo();

                            break;
                        case 2:

                            Title();
                            MenuFrameThree();
                            break;
                        case 3:

                            Title();
                            MenuFrameFour();
                            break;
                        case 4:

                            Title();
                            MenuFrameFive();
                            break;
                        case 5:
                            Title();
                            MenuFrameSix();
                            break;
                    }


                    keyPress = Console.ReadKey();
                    if (keyPress.Key == ConsoleKey.UpArrow)//Decrease the value of count if the Up Arrow is pressed
                    {
                        choice--;
                    }
                    else if (keyPress.Key == ConsoleKey.DownArrow)//Increase the value of count if the Down Arrow is pressed
                    {
                        choice++;
                    }
                    Console.Clear();
                    if (choice < 0) //if choice goes below 0, sets choice at 6
                    {
                        choice = 6;
                    }
                }
                while (keyPress.Key != ConsoleKey.Enter); //Runs untill user presses the Enter key

                switch (choice % 6) //Runs the menu option method depending on the value of count
                {
                    case 0:

                        SortByName();
                        ShowContestants();
                        break;
                    case 1:
                        SearchContestants();
                        break;
                    case 2:
                        ChooseFinalists();
                        break;
                    case 3:
                        ClearHighScores();
                        break;
                    case 4:
                        PickPlayer();
                        break;
                    case 5:
                        quit = true;
                        break;
                }
            }
            while (quit == false); //Whole menu runs untill the user selects quit
            SaveAndExit();
        }

        public static void ImportPlayers()
        {
            StreamReader readFile = new StreamReader(@"Resources\dealOrNoDeal.txt");
            int i = 0;
            arraySize = Convert.ToInt32(readFile.ReadLine()); //Reads in the first value from dealOrNoDeal.txt which is the length of the contestantList Array

            Array.Resize(ref contestantList, arraySize); //Resizes the array to the length defined above

            while (!readFile.EndOfStream) //Reads each line from dealOrNoDeal.txt into the contestantList Array
            {
                contestantList[i].lastName = readFile.ReadLine();
                contestantList[i].firstName = readFile.ReadLine();
                contestantList[i].interests = readFile.ReadLine();
                contestantList[i].highScore = Convert.ToDouble(readFile.ReadLine());
                i++;
            }
            readFile.Close();


        }

        public static void SortByName() //Bubble Sort
        {
            Contestant temp;
            for (int pass = 0; pass < contestantList.Length - 1; pass++)
            {
                for (int pos = 0; pos < contestantList.Length - 1; pos++)
                {
                    if (contestantList[pos + 1].lastName.CompareTo(contestantList[pos].lastName) < 0)
                    {
                        temp = contestantList[pos + 1];
                        contestantList[pos + 1] = contestantList[pos];
                        contestantList[pos] = temp;
                    }
                }

            }
        }

        public static void ShowContestants()
        {
            ShortTitle();
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**  These are the contestants:                                               **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**  Last Name:\t\tFirst Name:\tInterests:\t\tHigh Score:  **");
            Console.WriteLine("**                                                                           **");
            for (int i = 0; i < contestantList.Length; i++) //Lists each contestant
            {
                Console.WriteLine("**  {0}{1}{2}{3}**", contestantList[i].lastName.PadRight(20), contestantList[i].firstName.PadRight(16), contestantList[i].interests.PadRight(24), contestantList[i].highScore.ToString("C").PadRight(13));

                Thread.Sleep(50);
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                  Press Enter to Return to the Menu...                     **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
            Console.Clear();
        }

        public static void SearchContestants()
        {
            
            int count = 0;
            
            do //Runs untill ESC is pressed
            {
                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**  Please select the contestant you wish to edit:                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**  Last Name:\t\tFirst Name:\tInterests:\t\tHigh Score:  **");
                Console.WriteLine("**                                                                           **");
                for (int i = 0; i < contestantList.Length; i++)
                {
                    if (count % contestantList.Length == i)//Highlights the player that count % contestantList.Length is equivalent to
                    {
                        Console.Write("**  ");
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.Write("{0}{1}{2}{3}", contestantList[i].lastName.PadRight(20), contestantList[i].firstName.PadRight(16), contestantList[i].interests.PadRight(24), contestantList[i].highScore.ToString("C").PadRight(11));
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("  **");
                    }
                    else
                    {
                        Console.WriteLine("**  {0}{1}{2}{3}**", contestantList[i].lastName.PadRight(20), contestantList[i].firstName.PadRight(16), contestantList[i].interests.PadRight(24), contestantList[i].highScore.ToString("C").PadRight(13));
                    }
                }
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**           Use the Up and Down Arrows to make your choice.                 **");
                Console.WriteLine("**                Press Enter to confirm your choice.                        **");
                Console.WriteLine("**                                OR                                         **");
                Console.WriteLine("**    If you wish to add or remove contestants use the '+' and '-' keys.     **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                 Press Esc to return to the menu...                        **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                keyPress = Console.ReadKey();

                switch (keyPress.Key)//Runs different code depending on the value of count
                {
                    case ConsoleKey.UpArrow:
                        count--;
                        break;
                    case ConsoleKey.DownArrow:
                        count++;
                        break;
                    case ConsoleKey.Add:
                        AddContestant();
                        break;
                    case ConsoleKey.Subtract:
                        RemoveContestant((count % contestantList.Length));
                        break;
                    case ConsoleKey.Enter:
                        EditContestant((count % contestantList.Length));
                        break;
                }
                if (count < 0)
                {
                    count = contestantList.Length;
                }
                Console.Clear();
            }
            while (keyPress.Key != ConsoleKey.Escape);
        }

        public static void AddContestant()
        {
            Console.Clear();
            ShortTitle();
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.Write("**  How Many Contestants Are You Adding? ");
            int num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("**                                                                           **");

            int firstNewContest = contestantList.Length;//Assigns the old length of contestantList to firstNewContestant
            Array.Resize(ref contestantList, (contestantList.Length + num)); //Resizes the array to be the old size plus the number of people being added

            for (int i = firstNewContest; i < contestantList.Length; i++) //runs from the old length till the new length
            {
                Console.WriteLine("**  Please Enter the New Contestants Information: Below:                     **");
                Console.WriteLine("**                                                                           **");
                Console.Write("**  First Name: ");
                contestantList[i].firstName = Console.ReadLine();
                Console.Write("**  Last Name: ");
                contestantList[i].lastName = Console.ReadLine();
                Console.Write("**  Interests: ");
                contestantList[i].interests = Console.ReadLine();
                contestantList[i].highScore = 0; //Sets the new players high score at 0
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                Thread.Sleep(100);
                Console.Clear();
            }
            ShortTitle();
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                          Players Added!                                   **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            SortByName();
            Thread.Sleep(500);
        }

        public static void RemoveContestant(int contestantNumber) //contestantNumber is the Array position that is going to be removed
        {
            string border = "**";
            Contestant temp;
            Console.Clear();
            int choice = 1;
            do //Runs untill Enter is pressed
            {
                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.Write("**  Are you sure you wish to delete {0} {1}", contestantList[contestantNumber].firstName, contestantList[contestantNumber].lastName.PadRight(14));
                if (choice % 2 == 0) //Highlights yes
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write("  Yes  ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write("  No  ");
                    Console.WriteLine(border.PadLeft(10));
                }
                else if (choice % 2 == 1) //Highlights No
                {
                    Console.Write("  Yes  ");
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write("  No  ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.WriteLine(border.PadLeft(10));
                }
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**              Use the Left and right arrows to select                      **");
                Console.WriteLine("**                     Press Enter to Select...                              **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                switch (keyPress.Key)
                {
                    case ConsoleKey.LeftArrow:
                        choice--;
                        break;
                    case ConsoleKey.RightArrow:
                        choice++;
                        break;
                }
                if (choice < 0)
                {
                    choice = 2;
                }
                keyPress = Console.ReadKey();
                Console.Clear();
            }
            while (keyPress.Key != ConsoleKey.Enter);
            if (choice % 2 == 0) //If the choice was Yes then delete contestant
            {
                //Shifts the chosen contestant to the end of the Array
                temp = contestantList[(contestantList.Length - 1)];
                contestantList[(contestantList.Length - 1)] = contestantList[contestantNumber];
                contestantList[contestantNumber] = temp;

                Array.Resize(ref contestantList, (contestantList.Length - 1)); //Decreases the size of the array by one, thus removing the last contestant
                SortByName(); //Resorts the contestantList Array
                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                            !Contestant Deleted!                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");

            }
            else
            {
                ShortTitle();
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                Press Enter to Return to Previous Menu...                  **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
        }

        public static void EditContestant(int number) //number is the position in the array of the contestant being edited
        {
            Console.Clear();
            int i = number;
            int count = 0;
            bool edit = false;
            do
            {
                ShortTitle();
                edit = false;
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**  The Contestant's Previous Information was:                               **");
                Console.WriteLine("**  Last Name: {0}**", contestantList[i].lastName.PadRight(62));
                Console.WriteLine("**  First Name: {0}**", contestantList[i].firstName.PadRight(61));
                Console.WriteLine("**  Interests: {0}**", contestantList[i].interests.PadRight(62));
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**  What would you like to change?                                           **");
                Console.WriteLine("**                                                                           **");
                switch (count % 3) //Highlights the different selections
                {
                    case 0:
                        Console.Write("** ");
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.Write(" Last Name  ");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("                                                              **");
                        Console.WriteLine("**  First Name                                                               **");
                        Console.WriteLine("**  Interests                                                                **");
                        break;
                    case 1:
                        Console.Write("**  Last Name                                                                **\n** ");
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.Write(" First Name  ");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("                                                             **");
                        Console.WriteLine("**  Interests                                                                **");
                        break;
                    case 2:
                        Console.WriteLine("**  Last Name                                                                **");
                        Console.Write("**  First Name                                                               **\n** ");
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.Write(" Interests  ");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("                                                              **");
                        break;
                }
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                Use the Up and Down Arrows to select.                      **");
                Console.WriteLine("**                      Press Enter to Confirm.                              **");
                Console.WriteLine("**                Press Esc to Return to Previous Menu.                      **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                keyPress = Console.ReadKey();
                switch (keyPress.Key)
                {
                    case ConsoleKey.UpArrow:
                        count--;
                        break;
                    case ConsoleKey.DownArrow:
                        count++;
                        break;
                    case ConsoleKey.Enter:
                        edit = true;
                        break;
                }
                if (edit == true)
                {
                    switch (count % 3)
                    {
                        case 0:
                            Console.Write("\nNew Last Name: ");
                            contestantList[i].lastName = Console.ReadLine();
                            break;
                        case 1:
                            Console.Write("\nNew First Name: ");
                            contestantList[i].firstName = Console.ReadLine();
                            break;
                        case 2:
                            Console.Write("\nNew Interests: ");
                            contestantList[i].interests = Console.ReadLine();
                            break;
                    }
                }
                if (count < 0)
                {
                    count = 3;
                }
                Console.Clear();
            }
            while (keyPress.Key != ConsoleKey.Escape);
            SortByName();
        }

        public static void ChooseFinalists()
        {
            string border = "**";
            ShortTitle();
            if (finalistsChosen == false)
            {

                Random rand = new Random();

                for (int i = 0; i < finalists.Length; i++) //Randomly chooses positions in contestantList and places them in finalists array
                {
                    int randomNumber = rand.Next(0, contestantList.Length);
                    while (finalists.Contains(contestantList[randomNumber]))
                    {
                        randomNumber = rand.Next(0, contestantList.Length);
                    }
                    finalists[i] = contestantList[randomNumber];
                }
                finalistsChosen = true;
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                          Choosing Finalists...                            **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Thread.Sleep(500);
                Console.WriteLine("**  The Finalists Are:                                                       **");
                Console.WriteLine("**                                                                           **");
                for (int i = 0; i < finalists.Length; i++) //Displays each chosen finalist with a pause
                {
                    Console.WriteLine("**  Finalist No. {0}:    {1} {2}{3}", (i + 1).ToString().PadRight(2), finalists[i].firstName.PadRight(13), finalists[i].lastName.PadRight(15), border.PadLeft(26));
                    Console.WriteLine("**                                                                           **");
                    Thread.Sleep(250);
                }
            }
            else //Runs if finalistsChosen is true
            {
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**  The Finalists Are:                                                       **");
                Console.WriteLine("**                                                                           **");
                for (int i = 0; i < finalists.Length; i++) //Shows all finalists without the pause
                {
                    Console.WriteLine("**  Finalist No. {0}:    {1} {2}{3}", (i + 1).ToString().PadRight(2), finalists[i].firstName.PadRight(13), finalists[i].lastName.PadRight(15), border.PadLeft(26));
                    Console.WriteLine("**                                                                           **");
                }
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                  Press Enter to Return to the Menu...                     **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
            Console.Clear();
        }

        public static void ClearHighScores() //Almost the same as RemoveContestant method
        {
            string border = "**";
            Console.Clear();
            int choice = 1;
            do
            {
                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.Write("**  Are you sure you wish to delete the High Scores?");
                if (choice % 2 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write("  Yes  ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write("  No  ");
                    Console.WriteLine(border.PadLeft(14));
                }
                else if (choice % 2 == 1)
                {
                    Console.Write("  Yes  ");
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write("  No  ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.WriteLine(border.PadLeft(14));
                }
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**              Use the Left and right arrows to select                      **");
                Console.WriteLine("**                     Press Enter to Select...                              **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                keyPress = Console.ReadKey();
                switch (keyPress.Key)
                {
                    case ConsoleKey.LeftArrow:
                        choice--;
                        break;
                    case ConsoleKey.RightArrow:
                        choice++;
                        break;
                }
                if (choice < 1)
                {
                    choice = 2;
                }

                Console.Clear();
            }
            while (keyPress.Key != ConsoleKey.Enter);
            if (choice % 2 == 0)
            {
                for (int i = 0; i < contestantList.Length; i++) //sets all high scores to 0
                {
                    contestantList[i].highScore = 0;
                }
                SortByName();
                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                           !High Scores Deleted!                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");

            }
            else
            {
                ShortTitle();
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                Press Enter to Return to Previous Menu...                  **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
            Console.Clear();
        }

        public static void SaveAndExit()
        {
            ShortTitle();

            StreamWriter writeFile = new StreamWriter(@"Resources\dealOrNoDeal.txt");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                              Saving...                                    **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Thread.Sleep(500);
            writeFile.WriteLine(contestantList.Length); //Writes the size of the contestantList array to the first line in dealOrNoDeal.txt this is used next time the program is run to define the size of the Array in that instance
            for (int i = 0; i < contestantList.Length; i++) // Writes each contestants information to dealOrNoDeal.txt
            {
                writeFile.WriteLine(contestantList[i].lastName);
                writeFile.WriteLine(contestantList[i].firstName);
                writeFile.WriteLine(contestantList[i].interests);
                writeFile.WriteLine("{0:F2}", contestantList[i].highScore);
            }

            writeFile.Close();
            Console.Clear();
            ShortTitle();
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                Saved!                                     **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                         Press Enter to Quit...                            **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
        }
        #endregion PreGame Methods

        #region Game Methods

        public static void PickPlayer()
        {
            if (finalistsChosen == true) //Only runs if the finalists have already been Chosen
            {

                string border = "**";
                Random rand = new Random();

                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");


                Console.WriteLine("**  Finalists:                                                               **");
                Console.WriteLine("**                                                                           **");
                for (int i = 0; i < finalists.Length; i++) //Displays all the finalists
                {
                    Console.WriteLine("**  Finalist No. {0}:    {1} {2}{3}", (i + 1).ToString().PadRight(2), finalists[i].firstName.PadRight(13), finalists[i].lastName.PadRight(15), border.PadLeft(26));
                    Console.WriteLine("**                                                                           **");
                }
                Console.WriteLine("**                 Press Enter to Begin the Selection...                     **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                Console.ReadLine();
                Console.Clear();
                int randNum = rand.Next(40); //Finalist is chosen here
                int pause = 10;

                for (int i = 30; i >= randNum % 10; i--) //Acts like a spinning wheel. Stopping on the finalist chosen above
                {
                    ShortTitle();
                    Console.WriteLine("**                                                                           **");
                    Console.WriteLine("**                                                                           **");
                    Console.WriteLine("**  Finalists:                                                               **");
                    Console.WriteLine("**                                                                           **");
                    for (int j = 0; j < finalists.Length; j++)
                    {
                        if (i % 10 == j) //Highlights the finalists in position j
                        {
                            Console.Write("**  ");
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.Write("Finalist No. {0}:    {1} {2}", (j + 1).ToString().PadRight(2), finalists[j].firstName.PadRight(13), finalists[j].lastName.PadRight(15));
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.WriteLine(border.PadLeft(26));
                        }
                        else
                        {
                            Console.WriteLine("**  Finalist No. {0}:    {1} {2}{3}", (j + 1).ToString().PadRight(2), finalists[j].firstName.PadRight(13), finalists[j].lastName.PadRight(15), border.PadLeft(26));
                        }
                        Console.WriteLine("**                                                                           **");
                    }
                    if (i > randNum % 10) //only runs if i is greater than randNum % 10
                    {
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("*******************************************************************************");
                        Console.WriteLine("*******************************************************************************");
                        Thread.Sleep(pause);
                        Console.Clear();
                        pause += 10; //Pause gets longer each iteration
                    }
                }
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**  The Finalist Is:    {0} {1}{2}", finalists[randNum % 10].firstName.PadRight(13), finalists[randNum % 10].lastName.PadRight(15), border.PadLeft(26));
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                   Press Enter to Continue to the Game...                  **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                Console.ReadLine();
                Console.Clear();
                thePlayer = finalists[randNum % 10];

                AssignCaseValues();
                RunGame();

            }
            else //Runs if finalists have not been chosen
            {
                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                  !Finalists Have Not Been Chosen Yet!                     **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**         Please Return to the Menu and Select 'Choose Finalists'           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                     Press Enter to Return to Menu...                      **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                Console.ReadLine();
                Console.Clear();
            }
        }

        public static void AssignCaseValues()
        {
            Random rand = new Random();

            for (int i = 0; i < cases.Length; i++) //Randomly chooses caseValues and assigns them to a case
            {
                int randomNumber = rand.Next(0, caseValues.Length);
                while (cases.Contains(caseValues[randomNumber]))
                {
                    randomNumber = rand.Next(0, caseValues.Length);
                }
                cases[i] = caseValues[randomNumber];
            }
        }

        public static void CaseScreen()
        {
            /* Displays the case screen depending on the state of the boolean arrays
             * This is messy but it was the only way i could get it to work without goto statements 
             * There are no further comments in this method*/

            ShortTitle();
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.Write("**  ");
            if (cashValuesRevealed[0] == false) { Console.Write("<     $0.01>   "); } else if (cashValuesRevealed[0] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<     $0.01>   "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[0] == false) { Console.Write("*****"); } else if (casesOpened[0] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[1] == false) { Console.Write("*****"); } else if (casesOpened[1] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[2] == false) { Console.Write("*****"); } else if (casesOpened[2] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[3] == false) { Console.Write("*****"); } else if (casesOpened[3] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[4] == false) { Console.Write("*****"); } else if (casesOpened[4] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[5] == false) { Console.Write("*****"); } else if (casesOpened[5] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[13] == false) { Console.Write("    <    $1,000>"); } else if (cashValuesRevealed[13] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("    <    $1,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[1] == false) { Console.Write("<        $1>   "); } else if (cashValuesRevealed[1] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<        $1>   "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[0] == false) { Console.Write("* 1 *"); } else if (casesOpened[0] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 1 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[1] == false) { Console.Write("* 2 *"); } else if (casesOpened[1] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 2 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[2] == false) { Console.Write("* 3 *"); } else if (casesOpened[2] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 3 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[3] == false) { Console.Write("* 4 *"); } else if (casesOpened[3] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 4 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[4] == false) { Console.Write("* 5 *"); } else if (casesOpened[4] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 5 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[5] == false) { Console.Write("* 6 *"); } else if (casesOpened[5] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 6 *"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[14] == false) { Console.Write("    <    $5,000>"); } else if (cashValuesRevealed[14] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("    <    $5,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[2] == false) { Console.Write("<        $5>   "); } else if (cashValuesRevealed[2] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<        $5>   "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[0] == false) { Console.Write("*****"); } else if (casesOpened[0] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[1] == false) { Console.Write("*****"); } else if (casesOpened[1] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[2] == false) { Console.Write("*****"); } else if (casesOpened[2] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[3] == false) { Console.Write("*****"); } else if (casesOpened[3] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[4] == false) { Console.Write("*****"); } else if (casesOpened[4] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[5] == false) { Console.Write("*****"); } else if (casesOpened[5] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[15] == false) { Console.Write("    <   $10,000>"); } else if (cashValuesRevealed[15] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("    <   $10,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[3] == false) { Console.Write("<       $10>"); } else if (cashValuesRevealed[3] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<       $10>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("                                               ");
            if (cashValuesRevealed[16] == false) { Console.Write("<   $25,000>"); } else if (cashValuesRevealed[16] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<   $25,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[4] == false) { Console.Write("<       $25>  "); } else if (cashValuesRevealed[4] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<       $25>  "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[6] == false) { Console.Write("*****"); } else if (casesOpened[6] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[7] == false) { Console.Write("*****"); } else if (casesOpened[7] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[8] == false) { Console.Write("*****"); } else if (casesOpened[8] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[9] == false) { Console.Write("******"); } else if (casesOpened[9] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[10] == false) { Console.Write("******"); } else if (casesOpened[10] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write(" ");
            if (casesOpened[11] == false) { Console.Write("******"); } else if (casesOpened[11] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[17] == false) { Console.Write("   <   $50,000>"); } else if (cashValuesRevealed[17] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("   <   $50,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[5] == false) { Console.Write("<       $50>  "); } else if (cashValuesRevealed[5] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<       $50>  "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[6] == false) { Console.Write("* 7 *"); } else if (casesOpened[6] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 7 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[7] == false) { Console.Write("* 8 *"); } else if (casesOpened[7] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 8 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[8] == false) { Console.Write("* 9 *"); } else if (casesOpened[8] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 9 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[9] == false) { Console.Write("* 10 *"); } else if (casesOpened[9] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 10 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[10] == false) { Console.Write("* 11 *"); } else if (casesOpened[10] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 11 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write(" ");
            if (casesOpened[11] == false) { Console.Write("* 12 *"); } else if (casesOpened[11] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 12 *"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[18] == false) { Console.Write("   <   $75,000>"); } else if (cashValuesRevealed[18] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("   <   $75,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[6] == false) { Console.Write("<       $75>  "); } else if (cashValuesRevealed[6] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<       $75>  "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[6] == false) { Console.Write("*****"); } else if (casesOpened[6] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[7] == false) { Console.Write("*****"); } else if (casesOpened[7] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[8] == false) { Console.Write("*****"); } else if (casesOpened[8] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("*****"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[9] == false) { Console.Write("******"); } else if (casesOpened[9] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[10] == false) { Console.Write("******"); } else if (casesOpened[10] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write(" ");
            if (casesOpened[11] == false) { Console.Write("******"); } else if (casesOpened[11] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[19] == false) { Console.Write("   <  $100,000>"); } else if (cashValuesRevealed[19] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("   <  $100,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[7] == false) { Console.Write("<      $100>"); } else if (cashValuesRevealed[7] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<      $100>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("                                               ");
            if (cashValuesRevealed[20] == false) { Console.Write("<  $200,000>"); } else if (cashValuesRevealed[20] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<  $200,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[8] == false) { Console.Write("<      $200>    "); } else if (cashValuesRevealed[8] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<      $200>    "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[12] == false) { Console.Write("******"); } else if (casesOpened[12] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[13] == false) { Console.Write("******"); } else if (casesOpened[13] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[14] == false) { Console.Write("******"); } else if (casesOpened[14] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[15] == false) { Console.Write("******"); } else if (casesOpened[15] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[16] == false) { Console.Write("******"); } else if (casesOpened[16] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[21] == false) { Console.Write("     <  $300,000>"); } else if (cashValuesRevealed[21] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("     <  $300,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[9] == false) { Console.Write("<      $300>    "); } else if (cashValuesRevealed[9] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<      $300>    "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[12] == false) { Console.Write("* 13 *"); } else if (casesOpened[12] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 13 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[13] == false) { Console.Write("* 14 *"); } else if (casesOpened[13] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 14 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[14] == false) { Console.Write("* 15 *"); } else if (casesOpened[14] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 15 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[15] == false) { Console.Write("* 16 *"); } else if (casesOpened[15] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 16 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[16] == false) { Console.Write("* 17 *"); } else if (casesOpened[16] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 17 *"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[22] == false) { Console.Write("     <  $400,000>"); } else if (cashValuesRevealed[22] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("     <  $400,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[10] == false) { Console.Write("<      $400>    "); } else if (cashValuesRevealed[10] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<      $400>    "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[12] == false) { Console.Write("******"); } else if (casesOpened[12] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[13] == false) { Console.Write("******"); } else if (casesOpened[13] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[14] == false) { Console.Write("******"); } else if (casesOpened[14] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[15] == false) { Console.Write("******"); } else if (casesOpened[15] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[16] == false) { Console.Write("******"); } else if (casesOpened[16] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[23] == false) { Console.Write("     <  $500,000>"); } else if (cashValuesRevealed[23] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("     <  $500,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[11] == false) { Console.Write("<      $500>"); } else if (cashValuesRevealed[11] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<      $500>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("                                               ");
            if (cashValuesRevealed[24] == false) { Console.Write("<  $750,000>"); } else if (cashValuesRevealed[24] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<  $750,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**  ");
            if (cashValuesRevealed[12] == false) { Console.Write("<      $750>    "); } else if (cashValuesRevealed[12] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("<      $750>    "); Console.ForegroundColor = ConsoleColor.White; }
            if (casesOpened[17] == false) { Console.Write("******"); } else if (casesOpened[17] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[18] == false) { Console.Write("******"); } else if (casesOpened[18] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[19] == false) { Console.Write("******"); } else if (casesOpened[19] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[20] == false) { Console.Write("******"); } else if (casesOpened[20] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[21] == false) { Console.Write("******"); } else if (casesOpened[21] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            if (cashValuesRevealed[25] == false) { Console.Write("     <$1,000,000>"); } else if (cashValuesRevealed[25] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("     <$1,000,000>"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("  **");

            Console.Write("**                  ");
            if (casesOpened[17] == false) { Console.Write("* 18 *"); } else if (casesOpened[17] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 18 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[18] == false) { Console.Write("* 19 *"); } else if (casesOpened[18] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 19 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[19] == false) { Console.Write("* 20 *"); } else if (casesOpened[19] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 20 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[20] == false) { Console.Write("* 21 *"); } else if (casesOpened[20] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 21 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[21] == false) { Console.Write("* 22 *"); } else if (casesOpened[21] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 22 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("                   **");

            Console.Write("**                  ");
            if (casesOpened[17] == false) { Console.Write("******"); } else if (casesOpened[17] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[18] == false) { Console.Write("******"); } else if (casesOpened[18] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[19] == false) { Console.Write("******"); } else if (casesOpened[19] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[20] == false) { Console.Write("******"); } else if (casesOpened[20] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[21] == false) { Console.Write("******"); } else if (casesOpened[21] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("                   **");

            Console.WriteLine("**                                                                           **");

            Console.Write("**                      ");
            if (casesOpened[22] == false) { Console.Write("******"); } else if (casesOpened[22] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[23] == false) { Console.Write("******"); } else if (casesOpened[23] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[24] == false) { Console.Write("******"); } else if (casesOpened[24] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[25] == false) { Console.Write("******"); } else if (casesOpened[25] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("                       **");

            Console.Write("**                      ");
            if (casesOpened[22] == false) { Console.Write("* 23 *"); } else if (casesOpened[22] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 23 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[23] == false) { Console.Write("* 24 *"); } else if (casesOpened[23] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 24 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[24] == false) { Console.Write("* 25 *"); } else if (casesOpened[24] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 25 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[25] == false) { Console.Write("* 26 *"); } else if (casesOpened[25] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("* 26 *"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("                       **");

            Console.Write("**                      ");
            if (casesOpened[22] == false) { Console.Write("******"); } else if (casesOpened[22] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[23] == false) { Console.Write("******"); } else if (casesOpened[23] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[24] == false) { Console.Write("******"); } else if (casesOpened[24] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.Write("  ");
            if (casesOpened[25] == false) { Console.Write("******"); } else if (casesOpened[25] == true) { Console.ForegroundColor = ConsoleColor.Black; Console.Write("******"); Console.ForegroundColor = ConsoleColor.White; }
            Console.WriteLine("                       **");

            Console.WriteLine("**                                                                           **");

        }

        public static void MyCase(int caseNumber)
        {
            //Displays the players chosen case
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                          Your Case:       **");
            Console.WriteLine("**                                                         ************      **");
            Console.WriteLine("**                                                         *          *      **");
            Console.WriteLine("**                                                         *    {0}    *      **", caseNumber.ToString().PadRight(2));
            Console.WriteLine("**                                                         *          *      **");
            Console.WriteLine("**                                                         ************      **");
            Console.WriteLine("**                                                                           **");
        }

        public static void ChooseCases(int playersCase, int caseCount, bool dealTaken)
        {
            int caseChoice, casesLeft = caseCount, cashValuePosition = 0;

            do //iterates untill caseLeft < 1 and the deal has not been taken
            {
                Console.Clear();

                CaseScreen();
                MyCase(playersCase);

                Console.WriteLine("**  You Have {0} Cases Left to Open:                                           **", casesLeft);
                Console.Write("**  Please Select a Case Number: ");

                try
                {
                    caseChoice = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    caseChoice = 666;
                }

                if (caseChoice == 666)
                {
                    InvalidChoice();
                }
                else if (caseChoice != playersCase)
                {
                    if (caseChoice > 26)
                    {
                        NumberToHigh();
                    }
                    else if (caseChoice < 0)
                    {
                        NumberToLow();
                    }
                    else if (casesOpened[caseChoice - 1] == true)
                    {
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("**                   !That Case Has Already Been Opened!                     **");
                        Console.WriteLine("**                      Press Enter to Choose Again                          **");
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("*******************************************************************************");
                        Console.WriteLine("*******************************************************************************");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("**                          !Opening The Case!                               **");
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("**                                                                           **");
                        Thread.Sleep(500);
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("**                           Case {0} Contained:                              **", caseChoice.ToString().PadRight(2));
                        Console.WriteLine("**                                {0}                            **", cases[caseChoice - 1].ToString("C").PadRight(15));
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("**                       Press Enter to Cotinue...                           **");
                        Console.WriteLine("**                                                                           **");
                        Console.WriteLine("*******************************************************************************");
                        Console.WriteLine("*******************************************************************************");
                        Console.ReadLine();
                        Console.Clear();
                        casesLeft--;
                        for (int i = 0; i < caseValues.Length; i++)
                        {
                            if (caseValues[i] == cases[caseChoice - 1])
                            {
                                cashValuePosition = i;
                            }
                        }

                        casesOpened[caseChoice - 1] = true;
                        cashValuesRevealed[cashValuePosition] = true;
                    }
                }
                else if (playersCase == caseChoice)
                {
                    Console.WriteLine("**                                                                           **");
                    Console.WriteLine("**                !You Chose Your Own Case Number!                           **");
                    Console.WriteLine("**                 Press Enter To Choose Again...                            **");
                    Console.WriteLine("**                                                                           **");
                    Console.WriteLine("**                                                                           **");
                    Console.WriteLine("*******************************************************************************");
                    Console.WriteLine("*******************************************************************************");
                    Console.ReadLine();
                    Console.Clear();
                }


            } while ((casesLeft >= 1) && (dealTaken == false));

        }

        public static void NumberToHigh()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                      !That Number Was to High!                            **");
            Console.WriteLine("**                     Press Enter to Choose Again                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
            Console.Clear();
        }

        public static void NumberToLow()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                      !That Number Was to Low!                             **");
            Console.WriteLine("**                     Press Enter to Choose Again                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
            Console.Clear();
        }

        public static double BankersOffer(int round, int caseCount)
        {
            //Calculates the value of the bankers offer, using the average of the values remaining and dividing it by a semi-fixed percentage
            Random rand = new Random();

            double[] basePercantages = { 0, 0.08, 0.15, 0.22, 0.37, 0.57, 0.70, 0.85, 0.90, 1.00 };
            double average = 0, offer;

            if (rand.Next(2) == 0)
            {
                basePercantages[round] += (rand.Next(1, 5) / 100);
            }
            else
            {
                basePercantages[round] -= (rand.Next(1, 5) / 100);
            }

            for (int i = 0; i < cases.Length; i++)
            {
                if (cashValuesRevealed[i] == false)
                {
                    average = average + caseValues[i];
                }
            }

            average = average / caseCount;

            offer = average * basePercantages[round];

            return offer;
        }

        public static bool DealOrNoDealDecision(double bankersOffer) //Code in here similar to RemoveContestant() and ClearHighScores() methods
        {
            Console.Clear();

            int count = 1;

            do
            {
                ShortTitle();
                Console.WriteLine("**                           !Bankers Offer!                                 **");
                Console.WriteLine("**      **********************************************************           **");
                Console.WriteLine("**      *                        {0}*           **", bankersOffer.ToString("C").PadRight(32));
                Console.WriteLine("**      **********************************************************           **");
                Console.WriteLine("**  What would you like to tell the banker?                                  **");
                Console.WriteLine("**                                                                           **");
                if (count % 2 == 0)
                {
                    Console.Write("**          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                      ****************           **");
                    Console.Write("**          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                      ****************           **");
                    Console.Write("**          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("**            **"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                      **            **           **");
                    Console.Write("**          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("**    DEAL    **"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("          OR          **  NO DEAL   **           **");
                    Console.Write("**          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("**            **"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                      **            **           **");
                    Console.Write("**          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                      ****************           **");
                    Console.Write("**          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("                      ****************           **");
                }
                else if (count % 2 == 1)
                {
                    Console.Write("**          ****************                      "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("           **");
                    Console.Write("**          ****************                      "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("           **");
                    Console.Write("**          **            **                      "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("**            **"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("           **");
                    Console.Write("**          **    DEAL    **          OR          "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("**  NO DEAL   **"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("           **");
                    Console.Write("**          **            **                      "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("**            **"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("           **");
                    Console.Write("**          ****************                      "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("           **");
                    Console.Write("**          ****************                      "); Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.Black; Console.Write("****************"); Console.BackgroundColor = ConsoleColor.Black; Console.ForegroundColor = ConsoleColor.White; Console.WriteLine("           **");
                }
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**              Use the Left and right arrows to select                      **");
                Console.WriteLine("**                     Press Enter to Select...                              **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("*******************************************************************************");
                Console.WriteLine("*******************************************************************************");
                keyPress = Console.ReadKey();
                switch (keyPress.Key)
                {
                    case ConsoleKey.LeftArrow:
                        count--;
                        break;
                    case ConsoleKey.RightArrow:
                        count++;
                        break;
                }

                if (count < 1)
                {
                    count = 2;
                }

                Console.Clear();
            }
            while (keyPress.Key != ConsoleKey.Enter);
            if (count % 2 == 0) //If the choice was Deal then finish the game
            {

                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                    You Chose To Accept The Deal                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");


            }
            else
            {
                ShortTitle();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                    You Chose To Refuse The Deal                           **");
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
            }
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                Press Enter to Return to Previous Menu...                  **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();

            if (count % 2 == 0)
            {
                return true; //Deal Accepted
            }
            else
            {
                return false; //Deal Rejected
            }
        }

        public static void OpenYourCase(int playersCase)
        {
            //Opens the players case
            ShortTitle();
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                         Opening Your Case...                              **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Thread.Sleep(1000);
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                       Your Case Contained:                                **");
            Console.WriteLine("**                                {0}**", cases[playersCase].ToString("C").PadRight(40));
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                       Press Enter to Continue...                          **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
            Console.Clear();

        }

        public static void RunGame()
        {
            int playersCase;
            double winningAmount;
            int caseInPlayCount = 26;
            int caseChoiceCount = 6;
            int roundCount = 1;
            double offer = 0;
            bool dealTaken = false;
            for (int i = 0; i < casesOpened.Length; i++)
            {
                casesOpened[i] = false;
                cashValuesRevealed[i] = false;
            }

            do
            {
                Console.Clear();
                CaseScreen();
                Console.WriteLine("**                                                                           **");
                Console.WriteLine("**                                                                           **");
                Console.Write("**  Please Choose Your Case: ");
                try
                {
                    playersCase = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    playersCase = 666;
                }

                if (playersCase == 666)
                {
                    InvalidChoice();
                }
                else if (playersCase > 26)
                {
                    NumberToHigh();
                }
                else if (playersCase < 0)
                {
                    NumberToLow();
                }
                else
                {
                    casesOpened[playersCase - 1] = true;
                }
                Console.Clear();

            } while (playersCase < 0 || playersCase > 26);

            do
            {
                caseInPlayCount = caseInPlayCount - caseChoiceCount;
                if (roundCount < 9)
                {
                    ChooseCases(playersCase, caseChoiceCount, dealTaken);
                    offer = BankersOffer(roundCount, caseInPlayCount);
                    dealTaken = DealOrNoDealDecision(offer);
                }



                if (caseChoiceCount > 1)
                {
                    caseChoiceCount--;
                }




                roundCount++;
                Console.Clear();
            }
            while ((dealTaken == false) && (roundCount <= 9));

            if (dealTaken == false || roundCount == 9)
            {
                winningAmount = cases[playersCase];
            }
            else
            {
                winningAmount = offer;
            }

            OpenYourCase(playersCase);

            ShortTitle();
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                          !Congratulations!                                **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                           You Won: {0}**", winningAmount.ToString("C").PadRight(36));
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                  Press Enter to Return to The Menu...                     **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");

            Console.ReadLine();
            Console.Clear();

            for (int i = 0; i < contestantList.Length; i++)
            {
                if (contestantList[i].firstName == thePlayer.firstName && contestantList[i].lastName == thePlayer.lastName)
                {
                    contestantList[i].highScore = winningAmount;
                }
            }

            finalistsChosen = false;


        }

        private static void InvalidChoice()
        {
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("**                            !Invalid Choice!                               **");
            Console.WriteLine("**                      Press Enter to Choose Again                          **");
            Console.WriteLine("**                                                                           **");
            Console.WriteLine("*******************************************************************************");
            Console.WriteLine("*******************************************************************************");
            Console.ReadLine();
        }

        #endregion Game Methods

        static void Main()
        {
            //Runs the menu
            Menu();
        }
    }
}